/** build.js
 * @description 建筑渲染
 */

import { placeData } from '../data/place'
import { indexToPos } from '../api/transform'
import build1 from '../../assets/build-1.png'
import build2 from '../../assets/build-2.png'
import build3 from '../../assets/build-3.png'

class Build {
    constructor(el, role, size, len) {
        this.el = el
        this.role = role
        this.ctx = el.getContext('2d')
        this.size = size
        this.len = len
    }
    rateToPath(rate) {
        let path = null
        switch (rate) {
            case 0.3: path = build1
                break
            case 0.6: path = build2
                break
            case 1.2: path = build3
                break
        }
        return path
    }
    clearAll() {
        this.ctx.clearRect(0, 0, this.size, this.size)
    }
    clear(index) {
        let { x, y } = indexToPos(index, this.size, this.len)
        this.ctx.clearRect(x, y, this.len, this.len)
    }
    drawStar(r, R, x, y) {
        this.ctx.beginPath();
        for (let i = 0; i < 5; i++) {
            this.ctx.lineTo(Math.cos((18 + i * 72) / 180 * Math.PI) * R + x,
                -Math.sin((18 + i * 72) / 180 * Math.PI) * R + y);
            this.ctx.lineTo(Math.cos((54 + i * 72) / 180 * Math.PI) * r + x,
                -Math.sin((54 + i * 72) / 180 * Math.PI) * r + y);
        }
        this.ctx.closePath();
        this.ctx.fill()
    }
    initImage(src) {
        return new Promise((resolve) => {
            const image = new Image()
            image.onload = () => {
                resolve(image)
            }
            image.src = src
        })
    }
    async render() {
        for (let i = 0; i < placeData.length; i++) {
            if (placeData[i].isBuy === true) {
                let path = this.rateToPath(placeData[i].rate)
                try {
                    let image = await this.initImage(path)
                    let { x, y } = indexToPos(i, this.size, this.len)

                    this.ctx.drawImage(image, x, y, this.len, this.len)

                    if (this.role._role[this.role._current].id === placeData[i].buyRole) {
                        this.ctx.fillStyle = this.role._role[this.role._current].color
                        this.drawStar(5, 10, x + 12, y + 12)
                    }
    
                    this.ctx.font = '12px 宋体'
                    this.ctx.fillStyle = 'white'
                    this.ctx.fillText(`${placeData[i].buyRole + 1}`, x + 10, y + 16)
                } catch(e) {
                    console.error(e);
                }
              
            }
        }
    }
}

export default Build